from app import app,admin
from app import stripe_keys,models,db
import stripe
from flask import Flask, url_for, redirect, render_template, request, abort, flash
from flask_login import login_required, login_user, current_user,logout_user,LoginManager
from flask_admin import Admin, BaseView, expose, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.view import func
from flask_admin.menu import MenuLink
from flask_admin.model.form import InlineFormAdmin
from app.forms import ExecuteForm,ApplicationForm,QualificationForm,LoginForm,StudentForm, Inputs2,ExecuteForm2
from app import authenticate
import logging
import datetime
from datetime import datetime,timedelta,date,time
import os.path
from os import path
from functools import wraps
import _datetime
import random
import numpy as np
from csv import reader
import time


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            print(current_user)
            if not current_user.is_authenticated:
                print("user not authenticated")
                return abort(403)
            urole = login
            if ( (current_user.role != role) and (role != "ANY")):
                print("invalid user")
                return abort(403)
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


# @login_required(role="Admin")
class ExecuteView(BaseView):
    @expose('/', methods=['POST','GET'])
    def exe(self,*args, **kwargs):
        form = ExecuteForm2(request.form,meta={'csrf': True})
        print(form.errors)
        # if form.is_submitted():
        #     print("submitted")
        if form.validate_on_submit():
            print("Form Validate")
            if form.myField.data == "EF":
                exactFit()
                # print("EF")
            if form.myField.data == "EFR":
                exactFitwithMinReq()
                # print("EFR")
            if form.myField.data == "PR":
                process(form.multiplier.data)
            if form.myField.data == "CR":
                courseRankAlg(form.multiplier.data)
                # print(form.multiplier.data)
                # print("PR")
            return redirect(url_for("admin.index"))
        return self.render("admin/execute.html",form = form)
    def is_accessible(self):
           if current_user.is_authenticated:
               if current_user.role == "Admin":

                   return (current_user.is_active and
                           current_user.is_authenticated)
               else:
                   return abort(403)
           else:
               return abort(403)

# Verifies if matchings made by the alforithm are stable
class VerifyView(BaseView):
    @expose('/', methods=['POST','GET'])
    def verify(self,*args, **kwargs):

        # allStudents = models.User.query.filter_by(role = "Student").all()
        #get all enrolled sudents
        enrolledStudents = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").all()
        verification = "stable"
        #for each student in enrolledStudents
        for student in enrolledStudents:
            enrolledCourse = student.course
            studentPreferences = student.preferences.split()
            # print(studentPreferences)
            StudentsAppliedToCourseNotEnrolled = models.User.query.filter_by(role = "Student", appliedCourse = student.appliedCourse, enrolledCourse = None).all()
            studentIndex = studentPreferences.index(str(enrolledCourse.rank) )
            # studentsEnrolledAtCourse = models.User.query.filter_by(role = "Student", enrolledCourse = student.Course).all()
            StudentsAppliedandEnrolledToCourse = models.User.query.filter_by(role = "Student", appliedCourse = student.appliedCourse, enrolledCourse = enrolledCourse.rank).all()
            for std in StudentsAppliedandEnrolledToCourse:
                stdCourse = std.course
                stdPreferences = std.preferences.split()
                # stdIndex = stdPreferences.index(str(stdCourse.rank))
                studentIndex2 = studentPreferences.index(str(stdCourse.rank) )
                # if index of other enrolled student's course in first student's list is higher than other enrolled student
                # then they got a preference lower in their list than the other enrolled student

                if (studentIndex > studentIndex2) and (student.credits > std.credits):
                    print("UNSTABLE MATCHING!!!! PANIC!!!")
                    verification = "unstable"
                    return self.render("admin/verify.html",verification = verification)
            for std in StudentsAppliedToCourseNotEnrolled:
                if student.credits < std.credits:
                    print("UNSTABLE MATCHING!!!! PANIC!!!")
                    verification = "unstable"
                    return self.render("admin/verify.html",verification = verification)
            # for std in StudentsAppliedToCourse:

        return self.render("admin/verify.html",verification = verification)

    def is_accessible(self):
           if current_user.is_authenticated:
               if current_user.role == "Admin":

                   return (current_user.is_active and
                           current_user.is_authenticated)
               else:
                   return abort(403)
           else:
               return abort(403)

admin.add_view(VerifyView(name="Verification",endpoint="verify"))
admin.add_view(ExecuteView(name="Execute Algorithm",endpoint="execute"))

# resets the database and creates multiplier*NofSeats students
def resetdb(multiplier):

    #get all universities and courses
    unis = models.University.query.all()
    courses = models.Course.query.all()
    #delete all universities
    for uni in unis:
        db.session.delete(uni)
    #delete all courses
    for course in courses:
        db.session.delete(course)
    db.session.commit()

    #import universities & courses from file
    createUniversities()

    #compute sum of seats available
    all = models.User.query.filter_by(role = "Student")
    for student in all:
        db.session.delete(student)
    seats = 0
    courses = models.Course.query.all()
    for course in courses:
        seats += course.seats
    stdNo = seats
    print(stdNo)

    #create students
    if multiplier == 0 or multiplier is None :
        createStudents(stdNo)
    else:
        print("creating Students")
        createStudents(multiplier*stdNo)

# Flask views
@app.route('/reset')
@login_required(role="Admin")
def reset():
    # db.drop_all()
    # db.create_all()
    resetdb(0)
    return redirect(url_for("test"))
#
import math

def eu(m,n):
    if m ==0:
        return n,0,1
    else:
        r = n -math.floor(n/m)*m
        k,l,g = eu(r,m)
        s = g - math.floor(n/m) * l
        return k,s,l


#home view without logging
@app.route('/')
def index():
    if models.User.query.filter_by(role ="Admin").first() is None:
        authenticate.createAdmin("Stefanos","Costa","sc18sc@leeds.ac.uk","12345")
    if current_user.is_authenticated and current_user.role =="Student":
        return redirect(url_for('home'))
    elif current_user.is_authenticated and current_user.role =="Admin":
        return redirect(url_for('admin.index'))
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    # k,s,l = eu(19,300)
    # print(k)
    # print(s)
    # print(l)
    #
    # t = False
    # num = 0
    # while t is False:
    #      if 5*num % 352 == 1:
    #          print(num)
    #          t= True
    #      else:
    #         num+=1
    # print(current_user)
    # print("current user.is_authenticated" + " " + str(current_user.is_authenticated))
    # # x = 1
    # while t == False:
    #     if x**3525 % 391 == x:
    #         print(x)
    #         t = True
    #     else:
    #         t+=1
    # uniNo = models.University.query.count()
    # std = models.Student.query.filter(models.Student.enrolledCourse == None, models.Student.index != uniNo).first()
    # if std is not None:
    #     print(std.index)
    # else:
    #     print("is none")
    # studentgrades = []
    # for x in alevels:
    #     if std.grade1 == x[0]:
    #         studentgrades.append(x[1])
    #         break
    # for x in alevels:
    #     if std.grade2 == x[0]:
    #         studentgrades.append(x[1])
    #         break
    # for x in alevels:
    #     if std.grade3 == x[0]:
    #         studentgrades.append(x[1])
    #         break
    # sum = 0
    # for i in studentgrades:
    #     sum += i
    # print(sum)
    # course = models.Course.query.filter_by(rank = 1).first()
    # for std in course.students:
    #     print(std.name)
    return render_template('home.html', title = 'Home')
#

@app.route('/MyApplication')
@login_required(role="Student")
def MyApplication():
    print(current_user.appliedCourse)
    print(current_user.course)
    course = models.Course.query.filter_by(courseId = current_user.enrolledCourse).first()
    if course != None:
        uni = models.University.query.filter_by(uniId = course.university_id).first()
    else:
        uni = ""
    return render_template('myApp.html', title = 'My Application (UG)',course = course,uni = uni)
#


@app.route('/results')
def results():
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    # print(current_user.appliedCourse)
    # for i in range(26):
    #     print(str(i) + " " + str((i*17)%26))

    studentNo = models.User.query.filter(models.User.appliedCourse != None,models.User.role == "Student").count()
    print(studentNo)
    total_enrolled = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").count()
    if studentNo != 0:
        penrolled = total_enrolled/studentNo * 100
    else:
        penrolled = 0
    penrolled = str(penrolled) + "%"
    enrolledStudents = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").all()
    # for student in enrolledStudents:
    uniNo = models.University.query.count()
    # students = models.Student.query.filter_by(models.Student.enrolledCourse !=Nonw)
    unis = models.University.query.all()
    courses = models.Course.query.all()
    nStudentsFulfillorHigher = 0
    nStudentsFulfill = 0
    #for each student
    for student in enrolledStudents:
        enrolledCourse = student.course
        if enrolledCourse != None:
            courseCriteria =[enrolledCourse.req1,enrolledCourse.req2,enrolledCourse.req3]
            studentCriteria = [student.grade1,student.grade2,student.grade3]
            courseCriteria.sort()
            studentCriteria.sort()
            # if their qualifications match course criteria exactly +1
            if  courseCriteria ==studentCriteria:
                nStudentsFulfill+=1
            # calculate grades for student
            studentgrades = []
            for x in alevels:
                if student.grade1 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade2 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade3 == x[0]:
                    studentgrades.append(x[1])
                    break
            studentSum = 0
            for i in studentgrades:
                studentSum += i

            #calculate sum of grades student needs for this course
            coursegrades = []
            for x in alevels:
                if enrolledCourse.req1 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if enrolledCourse.req2 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if enrolledCourse.req3 == x[0]:
                    coursegrades.append(x[1])
                    break
            gradeSum = 0
            for i in coursegrades:
                gradeSum += i
            if studentSum >=gradeSum:
                nStudentsFulfillorHigher +=1
    seats = 0
    # studentsEnrolledAtAppliedCourse = 0
    courseData = []
    nStudentsmatch = []
    for course in courses:
        nStudentsExactMatchAcourse = 0
        nStudentsMatchAcourse = 0
        nStudentsEnrolledAtMatchedCourse = 0
        nStudentsEnrolledAtExactMatchedCourse = 0
        studentsEnrolledAtAppliedCourse = 0
        sEnrolled = models.User.query.filter_by(enrolledCourse = course.courseId, role = "Student").all()
        studentsEnrolled = models.User.query.filter(models.User.enrolledCourse == course.courseId,models.User.role == "Student").count()
        nStudentsApplied = models.User.query.filter_by(appliedCourse = course.CourseName,role = "Student").all()
        seats += course.seats


        for student in sEnrolled:
            # print(student)
            # print("HEREEE")
            stdgrades = [student.grade1, student.grade2, student.grade3]
            coursereq = [course.req1, course.req2, course.req3]
            coursereq.sort()
            stdgrades.sort()
            if coursereq == stdgrades:
                # print("exactmatch")
                nStudentsEnrolledAtExactMatchedCourse+=1

            studentgrades = []
            for x in alevels:
                if student.grade1 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade2 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade3 == x[0]:
                    studentgrades.append(x[1])
                    break
            studentSum = 0
            for i in studentgrades:
                studentSum += i

            coursegrades = []
            for x in alevels:
                if course.req1 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if course.req2 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if course.req3 == x[0]:
                    coursegrades.append(x[1])
                    break
            gradeSum = 0
            for i in coursegrades:
                gradeSum += i
            if studentSum >=gradeSum:
                # print("range match")
                nStudentsEnrolledAtMatchedCourse +=1


        for student in nStudentsApplied:

            # print(course)
            # print(student)
            stdgrades = [student.grade1, student.grade2, student.grade3]
            coursereq = [course.req1, course.req2, course.req3]
            coursereq.sort()
            stdgrades.sort()
            if coursereq == stdgrades:
                nStudentsExactMatchAcourse+=1

            studentgrades = []
            for x in alevels:
                if student.grade1 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade2 == x[0]:
                    studentgrades.append(x[1])
                    break
            for x in alevels:
                if student.grade3 == x[0]:
                    studentgrades.append(x[1])
                    break
            studentSum = 0
            for i in studentgrades:
                studentSum += i
            coursegrades = []
            for x in alevels:
                if course.req1 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if course.req2 == x[0]:
                    coursegrades.append(x[1])
                    break
            for x in alevels:
                if course.req3 == x[0]:
                    coursegrades.append(x[1])
                    break
            gradeSum = 0
            for i in coursegrades:
                gradeSum += i
            if studentSum >=gradeSum:
                # print(str(studentSum) + ">=" + str(gradeSum) )
                nStudentsMatchAcourse +=1

            if student.enrolledCourse != None:
                if student.appliedCourse == student.course.CourseName:
                    studentsEnrolledAtAppliedCourse += 1


        courseData.append((course.University,course.CourseName,studentsEnrolled, course.seats,nStudentsExactMatchAcourse, nStudentsMatchAcourse,nStudentsEnrolledAtExactMatchedCourse,nStudentsEnrolledAtMatchedCourse))
    print(courseData)

    # u = models.University.query.filter_by(uniName = "University of Nottingham").first()
    # c = models.Course.query.filter_by(CourseName = "Economics",university_id =u.uniId).first()
    # n = models.Student.query.filter(models.Student.enrolledCourse == c.courseId).all()
    # k = 0
    # for s in n:
    #     stdgrades = [s.grade1, s.grade2, s.grade3]
    #     coursereq = [c.req1, c.req2, c.req3]
    #     coursereq.sort()
    #     stdgrades.sort()
    #     print(str(stdgrades) + " " + str(coursereq))
    #     if stdgrades == coursereq:
    #         k+=1
    #
    # print(u)
    # print(c)
    # # print(n)
    # print(k)

    if studentNo !=0:
        pFulfilled = str(round(nStudentsFulfill/studentNo * 100)) + "%"
    else:
        pFulfilled = 0
    return render_template('results.html',
                            title = 'Algorithm results',
                            total = studentNo,
                            total_enrolled = total_enrolled,
                            percentage_enrolled = penrolled,
                            uniNo = uniNo,
                            nStudentsFulfill = nStudentsFulfill,
                            nStudentsFulfillorHigher = nStudentsFulfillorHigher,
                            seats = seats,
                            pFulfilled = pFulfilled,
                            courseData = courseData)


#generates preference lists for each student usinf university rankings
#and sets number of student per course equal to number of seats
def exactInit():

    allStudents = models.User.query.filter_by(role = "Student").all()
    allStudentsNo = models.User.query.filter_by(role = "Student").count()
    allUniversities = models.University.query.order_by(models.University.ranking.asc())
    studentPreferences = ""

    #create student preference list
    for uni in allUniversities:
        studentPreferences += str(uni.ranking) + " "
    #reset all students
    for student in allStudents:
        student.preferences = studentPreferences
        student.index = 0
        student.enrolledCourse = None

    #reset course availability
    courses = models.Course.query.all()
    ncourses = models.Course.query.count()
    for course in courses:
        course.availability = 0
    #get all distinct courses
    query = db.session.query(models.Course.CourseName.distinct().label("CourseName"))
    cs = [row.CourseName for row in query.all()]
    coursesAndSeats = []

    #calculate total seats available for each course and add each course-seat number tuple to array
    for co in cs:
        seats = 0
        ccs = models.Course.query.filter_by(CourseName = co).all()
        for j in ccs:
            seats += j.seats
        coursesAndSeats.append((co,seats))
    courseNo = 0
    # print(coursesAndSeats)

    #set each student's course so that # of course seats = number of students applying for that course
    for no in range(allStudentsNo):
        # print("Iteration " + str(no))
        sum = 0
        for k in range(courseNo+1):
            # print("k " + str(k))
            cor =coursesAndSeats[k]
            sum += cor[1]
        # print(sum)
            # print(no)
        if sum <= no:
            courseNo+=1
        appl = coursesAndSeats[courseNo]
        # print(appl)
        appliedCourse = appl[0]
        # print(allStudents[no])
        # print(appliedCourse)
        allStudents[no].appliedCourse = appliedCourse
    db.session.commit()

    #confirm numbers are correct by printing to console
    # print(coursesAndSeats)
    # for l in coursesAndSeats:
    #     ns = models.User.query.filter_by(appliedCourse = l[0],role = "Student").count()
    #     # print("hehe")
    #     print(ns)


# Gale-shapley algorithm for n Students by n Seats based on matching
#course criterira exactly.Student Preference lists based on university ranking
def exactFit():
    #initialise database for algorithm
    exactInit()
    count = 0
    #while we still have students that have not enrolled
    while models.User.query.filter_by(enrolledCourse = None,role = "Student").first() != None:
        # count +=1
        # print(count)
        total_enrolled = models.User.query.filter(models.User.enrolledCourse != None,role = "Student").count()
        # print(total_enrolled)
        # if count == 13352:
        #     break
        #get first student found not enrolled
        std = models.User.query.filter_by(enrolledCourse = None,role = "Student").first()
        print(std.name + " " + str(total_enrolled))
        #split student's preference list into array
        preference_list = std.preferences.split()
        # print(preference_list)

        #if student's list has not been exhausted
        if std.index < len(preference_list):
            #get university rank
            rank = int(preference_list[std.index])
            #get university and course for that uni rank
            uni = models.University.query.filter_by(ranking = rank).first()
            course = models.Course.query.filter_by(CourseName = std.appliedCourse, University = uni).first()

            # if student's appllied course exists at the current university
            if course != None:

                #if the course seats have not been exhausted add the student to the course
                if course.availability < course.seats:
                    course.availability +=1
                    std.enrolledCourse = course.courseId
                else:
                    #get student enrolled at the current course
                    studentsEnrolledAtCourse = models.User.query.filter_by(enrolledCourse = course.courseId,role = "Student").all()

                    #sort criteria in order
                    stdgrades = [std.grade1, std.grade2, std.grade3]
                    coursereq = [course.req1, course.req2, course.req3]
                    coursereq.sort()
                    stdgrades.sort()
                    #initialise count
                    stdcount = 0
                    #initialise if student that has applied
                    stdFit = False
                    studentFit = False
                    #if student matches criteria
                    if coursereq == stdgrades:
                        stdFit = True
                    # for i in range(len(stdgrades)):
                    #     if stdgrades[i] in coursereq:
                    #         stdcount+=1
                    #if a student fits the criteria and current student doesn't swap them
                    for student in studentsEnrolledAtCourse:
                        studentgrades = [student.grade1, student.grade2, student.grade3]
                        studentFit = False
                        studentCount = 0
                        studentgrades.sort()
                        if coursereq ==studentgrades:
                            studentFit = True
                        # for i in range(len(studentgrades)):
                        #     if studentgrades[i] == coursereq:
                        #         studentCount+=1
                        if stdFit and not studentFit:
                            std.enrolledCourse = course.courseId
                            student.enrolledCourse = None
                            break
        # assume the current index is checked and increment it for the next check
        std.index += 1
        db.session.commit()

# Gale-shapley algorithm for n Students by n Seats with course criteria based on having the highest qualifications
# Student Preference lists based on university ranking
def exactFitwithMinReq():
    exactInit()
    count = 0
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    #while we still have students that have not enrolled
    while models.User.query.filter_by(enrolledCourse = None,role = "Student").first() != None:
        # count +=1
        # print(count)
        total_enrolled = models.User.query.filter(models.User.enrolledCourse != None,role = "Student").count()
        # print(total_enrolled)
        # if count == 13352:
        #     break
        std = models.User.query.filter_by(enrolledCourse = None,role = "Student").first()
        print(std.studentName + " " + str(total_enrolled))
        preference_list = std.preferences.split()
        # print(preference_list)
        # print(std)

        #if student's list has not been exhausted
        if std.index < len(preference_list):
            rank = int(preference_list[std.index])
            uni = models.University.query.filter_by(ranking = rank).first()
            course = models.Course.query.filter_by(CourseName = std.appliedCourse, University = uni).first()

            # print("entered first if")
            # if student's appllied course exists at the current university
            if course != None:
                # print("entered second if")
                #if the course seats have not been exhausted add the student to the course
                if course.availability < course.seats:
                    course.availability +=1
                    std.enrolledCourse = course.courseId
                else:
                    #get students enrolled at the current course
                    studentsEnrolledAtCourse = models.User.query.filter_by(enrolledCourse = course.courseId,role ="Student").all()

                    #sort criteria in order
                    #compute curent student's grade total
                    stdgrades = []
                    for x in alevels:
                        if std.grade1 == x[0]:
                            stdgrades.append(x[1])
                            break
                    for x in alevels:
                        if std.grade2 == x[0]:
                            stdgrades.append(x[1])
                            break
                    for x in alevels:
                        if std.grade3 == x[0]:
                            stdgrades.append(x[1])
                            break
                    stdSum = 0
                    for i in stdgrades:
                        stdSum += i

                    #find student with minimum  grades enrolled in course
                    studentsInCourse = []
                    currentMin = None
                    for student in studentsEnrolledAtCourse:
                        if currentMin is None:
                            currentMin = student
                        else:
                            mingrades = []
                            for x in alevels:
                                if currentMin.grade1 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            for x in alevels:
                                if currentMin.grade2 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            for x in alevels:
                                if currentMin.grade3 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            minSum = 0
                            for i in mingrades:
                                minSum += i
                            studentgrades = []
                            for x in alevels:
                                if student.grade1 == x[0]:
                                    studentgrades.append(x[1])
                                    break
                            for x in alevels:
                                if student.grade2 == x[0]:
                                    studentgrades.append(x[1])
                                    break
                            for x in alevels:
                                if student.grade3 == x[0]:
                                    studentgrades.append(x[1])
                                    break

                            # print()
                            studentSum = 0
                            for i in studentgrades:
                                studentSum += i
                            # print(studentSum)
                            # print(minSum)
                            if minSum > studentSum:
                                currentMin = student
                    # print(str(minSum) + "<" + str(stdSum))
                    #if current student's qualifications are better than that
                    #of the student with the lowest criteria enrolled in the course, swap them
                    if minSum < stdSum:
                        std.enrolledCourse = course.courseId
                        currentMin.enrolledCourse = None
                    #initialise

                # assume the current index is checked and increment it for the next check
        std.index += 1
        db.session.commit()

def processInit():

    allStudents = models.User.query.filter_by(role = "Student").all()
    allStudentsNo = models.User.query.filter_by(role ="Student").count()
    allUniversities = models.University.query.order_by(models.University.ranking.asc())
    studentPreferences = ""
    #create preference list for all students
    for uni in allUniversities:
        studentPreferences += str(uni.ranking) + " "
    #set preference list for each student and initialuze them
    for student in allStudents:
        student.preferences = studentPreferences
        student.index = 0
        student.enrolledCourse = None
        # student.appliedCourse =
    courses = models.Course.query.all()
    ncourses = models.Course.query.count()
    #empty the courses
    for course in courses:
        course.availability = 0
    # query = db.session.query(models.Course.CourseName.distinct().label("CourseName"))
    # cs = [row.CourseName for row in query.all()]
    # coursesAndSeats = []
    # #
    # for co in cs:
    #     seats = 0
    #     ccs = models.Course.query.filter_by(CourseName = co).all()
    #     for j in ccs:
    #         seats += j.seats
    #     coursesAndSeats.append((co,seats))
    # courseNo = 0
    # print(coursesAndSeats)
    # for no in range(allStudentsNo):
    #     print("Iteration " + str(no))
    #     sum = 0
    #     for k in range(courseNo+1):
    #         print("k " + str(k))
    #         cor =coursesAndSeats[k]
    #         sum += cor[1]
    #     print(sum)
    #         # print(no)
    #     if sum <= no:
    #         courseNo+=1
    #     appl = coursesAndSeats[courseNo]
    #     print(appl)
    #     appliedCourse = appl[0]
    #     print(allStudents[no])
    #     print(appliedCourse)
    #     allStudents[no].appliedCourse = appliedCourse
    # for co in coursesAndSeats:
    db.session.commit()
    # print(coursesAndSeats)
    # for l in coursesAndSeats:
    #     ns = models.Student.query.filter_by(appliedCourse = l[0]).count()
    #     print("hehe")
    #     print(ns)


def process(multiplier):
    print(multiplier)
    pass
    resetdb(multiplier)
    processInit()
    count = 0
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    uniNo = models.University.query.count()
    #while we still have students that have not enrolled
    while models.User.query.filter(models.User.enrolledCourse == None, models.User.index != uniNo,models.User.role == "Student").first() != None:
        # count +=1
        # print(count)
        total_enrolled = models.User.query.filter(models.User.enrolledCourse != None, models.User.role =="Student").count()
        # print(total_enrolled)
        # if count == 13352:
        #     break
        std = models.User.query.filter(models.User.enrolledCourse == None, models.User.index != uniNo,models.User.role =="Student").first()
        print(std.name + " " + str(total_enrolled))
        preference_list = std.preferences.split()
        # print(preference_list)
        # print(std)
        #if student's list has not been exhausted
        if std.index < len(preference_list):
            rank = int(preference_list[std.index])
            uni = models.University.query.filter_by(ranking = rank).first()
            course = models.Course.query.filter_by(CourseName = std.appliedCourse, University = uni).first()

            # print("entered first if")
            # if student's appllied course exists at the current university
            if course != None:
                # print("entered second if")
                #if the course seats have not been exhausted add the student to the course
                if course.availability < course.seats:
                    course.availability +=1
                    std.enrolledCourse = course.courseId
                else:
                    #get students enrolled at the current course
                    studentsEnrolledAtCourse = models.User.query.filter_by(enrolledCourse = course.courseId,role = "Student").all()

                    #sort criteria in order
                    stdgrades = []
                    for x in alevels:
                        if std.grade1 == x[0]:
                            stdgrades.append(x[1])
                            break
                    for x in alevels:
                        if std.grade2 == x[0]:
                            stdgrades.append(x[1])
                            break
                    for x in alevels:
                        if std.grade3 == x[0]:
                            stdgrades.append(x[1])
                            break
                    stdSum = 0
                    for i in stdgrades:
                        stdSum += i


                    studentsInCourse = []
                    currentMin = None
                    for student in studentsEnrolledAtCourse:
                        if currentMin is None:
                            currentMin = student
                        else:
                            mingrades = []
                            for x in alevels:
                                if currentMin.grade1 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            for x in alevels:
                                if currentMin.grade2 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            for x in alevels:
                                if currentMin.grade3 == x[0]:
                                    mingrades.append(x[1])
                                    break
                            minSum = 0
                            for i in mingrades:
                                minSum += i
                            studentgrades = []
                            for x in alevels:
                                if student.grade1 == x[0]:
                                    studentgrades.append(x[1])
                                    break
                            for x in alevels:
                                if student.grade2 == x[0]:
                                    studentgrades.append(x[1])
                                    break
                            for x in alevels:
                                if student.grade3 == x[0]:
                                    studentgrades.append(x[1])
                                    break

                            # print()
                            studentSum = 0
                            for i in studentgrades:
                                studentSum += i
                            # print(studentSum)
                            # print(minSum)
                            if minSum > studentSum:
                                currentMin = student

                    if minSum < stdSum:
                        std.enrolledCourse = course.courseId
                        currentMin.enrolledCourse = None
                    # betterFound = True

                # assume the current index is checked and increment it for the next check
        std.index += 1
        db.session.commit()


def courseRankAlgInit():

    allStudents = models.User.query.filter(models.User.appliedCourse != None,models.User.role == "Student").all()
    allStudentsNo = models.User.query.filter(models.User.appliedCourse != None,models.User.role == "Student").count()
    # allCourses = models.Course.query.order_by(models.Course.rank.asc())
    studentPreferences = ""
    #create preference list for all students

    #set preference list for each student and initialize them
    for student in allStudents:
        studentPreferences = ""
        courses = models.Course.query.filter_by(CourseName = student.appliedCourse).order_by(models.Course.rank.asc())
        for c in courses:
            studentPreferences += str(c.rank) + " "
        student.preferences = studentPreferences
        student.index = 0
        student.enrolledCourse = None
        student.pLength = len(studentPreferences.split())

        # student.appliedCourse =
    allcourses = models.Course.query.all()
    ncourses = models.Course.query.count()
    #empty the courses
    for course in allcourses:
        course.availability = 0
    db.session.commit()


def courseRankAlg(multiplier):
    print(multiplier)
    pass
    resetdb(multiplier)
    print("Database Reset")
    courseRankAlgInit()
    count = 0
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    # uniNo = models.University.query.count()
    run = True
    start = time.time()
    studentsTested = 0
    print("entering Loop")
    #while we still have students that have not enrolled and their preference list has not been exhausted:
    while (std:=models.User.query.filter(models.User.enrolledCourse == None,models.User.index != models.User.pLength,models.User.role == "Student").first()) is not None:
        # #Get students that have not been enrolled yet
        # stdnts = models.User.query.filter(models.User.enrolledCourse == None,models.User.role == "Student").all()
        # #Get number of students that have not enrolled yet
        # n = models.User.query.filter(models.User.enrolledCourse == None,models.User.role == "Student").count()
        # countStudentsLeft = 0
        # #if number of students that have not enrolled yet > 1
        # if n > 1:
        #     #for each student in stdnts
        #     for s in stdnts:
        #         #if one of them has not exhausted their list yet
        #         if s.index == s.pLength:
        #             countStudentsLeft+=1
        #         else:
        #             std = s
        #             break
        # else:
        #     if stdnts.index == stdnts.plength:
        #         countStudentsLeft+=1
        #     else:
        #         std = stdnts
        # if countStudentsLeft == n:
        #     run =False
        # else:
        if std.index ==0:
            studentsTested+=1
            print(str(std.id) + " " + str(studentsTested))
        # total_enrolled = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").count()
        # print(total_enrolled)
        # if count == 13352:
        #     break
        # std = models.Student.query.filter(models.Student.enrolledCourse == None, models.Student.index != uniNo).first()
        # print(std.name + " " + str(total_enrolled))

        preference_list = std.preferences.split()
        # print(preference_list)
        # print(std)
        #if student's list has not been exhausted
        # if std.index < len(preference_list):
        rank = int(preference_list[std.index])
            # uni = models.University.query.filter_by(ranking = rank).first()
        course = models.Course.query.filter_by(rank = rank,CourseName = std.appliedCourse).first()

            # print("entered first if")
            # if student's appllied course exists at the current university
            # if course != None:
                # print("entered second if")
                #if the course seats have not been exhausted add the student to the course
        if course.availability < course.seats:
            course.availability +=1
            std.enrolledCourse = course.courseId
        else:
                    #get student enrolled at the current course
                    # studentsEnrolledAtCourse = models.User.query.filter_by(enrolledCourse = course.courseId,role = "Student").all()
                    # currentMin = models.User.query.filter_by(enrolledCourse = course.courseId,role = "Student").order_by(models.User.credits.asc()).first()

                    # studentsInCourse = []
            currentMin = None
                    # for student in studentsEnrolledAtCourse:
            for student in course.students:
                if currentMin is None:
                    currentMin = student
                else:

                    if currentMin.credits > student.credits:
                        currentMin = student


            if std.credits > currentMin.credits:
                std.enrolledCourse = course.courseId
                currentMin.enrolledCourse = None
            # betterFound = True

                    # assume the current index is checked and increment it for the next check
        std.index += 1
        db.session.commit()
    end = time.time()
    print(end-start)
    timed = end-start
    with open("time.txt","a") as file:
        file.write("\n" + str(end-start))

@app.route('/exe',methods = ['POST','GET'])
def exe():
    # alevels = [("A*",5),("A",4),("B",3),("C",2),("D",1)]
    form = ExecuteForm(request.form,meta={'csrf': True})
    print(form.errors)
    # if form.is_submitted():
    #     print("submitted")
    if form.validate_on_submit():
        print("Form Validate")
        # exactFit()
        # exactFitwithMinReq()
        process()
            # if std.index >4:
            #     print("found")
            #     break
        return redirect(url_for("results"))
        # db.session.query(models.Course.CourseName.distinct().label("CourseName"))
                    # enrolledStudentGrade1 = [item for item in alevels if item[0] == student.grade1]
                    # enrolledStudentGrade2 = [item for item in alevels if item[0] == student.grade2]
                    # enrolledStudentGrade3 = [item for item in alevels if item[0] == student.grade3]
                    # if std.grade1 >student.grade1:
    # print(current_user.appliedCourse)

    return render_template('execute.html', title = 'Execute algorithm?',form = form)
#

@app.route('/apply',methods = ["GET", "POST"])
@login_required(role="Student")
def apply():

    form = ApplicationForm(request.form,meta={'csrf': True})
    query = db.session.query(models.Course.CourseName.distinct().label("CourseName"))
    courses = [row.CourseName for row in query.all()]
    if form.validate_on_submit():
        std = models.User.query.filter_by(id = current_user.id).first()
        std.appliedCourse = form.courseField.data
        db.session.commit()
        return redirect(url_for("MyApplication"))
    return render_template('application.html', title = 'Choose a Course',form = form)


@app.route('/edit',methods = ['GET','POST'])
@login_required(role="Student")
def edit():
    form = QualificationForm(request.form,meta={'csrf': True})
    # print(current_user)
    # std = models.Student.query.filter_by(id = current_user.id)
    # print(std)?
    # subjectChoice = [("Latin", "Latin"), ("Classical Greek", "Classical Greek"), ("French", "French"), ("Italian", "Italian"), ("Classical Civilisations", "Classical Civilisations"), ("Law", "Law"), ("Music", "Music"), ("Philosophy", "Philosophy"), ("English Literature", "English Literature"),("English Language and Literature", "English Language and Literature"),
    #  ("History", "History"), ("Environmental Science", "Environmental Science"), ("Geography", "Geography"), ("Economics", "Economics"), ("Business Studies", "Business Studies"), ("Government and Politics", "Government and Politics"), ("Biology", "Biology"), ("Human Biology", "Human Biology"), ("Mathematics", "Mathematics"), ("Further Maths", "Further Maths"), ("Statistics", "Statistics"),
    #  ("Computing", "Computing"), ("Psychology", "Psychology"), ("Sociology", "Sociology"), ("Computer Science", "Computer Science"), ("Electronics", "Electronics"), ("Art", "Art"), ("Design and Technology", "Design and Technology"), ("Physics", "Physics"), ("Chemistry", "Chemistry"), ("History of Art", "History of Art"), ("Geology", "Geology"), ("ICT","ICT"), ("NA","None") ]
    alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    if form.validate_on_submit():

        # grade1 = form.myField.data
        std = models.User.query.filter_by(id = current_user.id).first()
        print("HERE")
        print( form.subjectField1.data)
        print( form.subjectField2.data)
        print( form.subjectField3.data)
        std.subject1 = form.subjectField1.data
        std.grade1 =  form.gradeField1.data
        std.subject2 = form.subjectField2.data
        std.grade2 =  form.gradeField2.data
        std.subject3 = form.subjectField3.data
        std.grade3 =  form.gradeField3.data
        stdgrades = []
        for x in alevels:
            if form.gradeField1.data == x[0]:
                stdgrades.append(x[1])
                break
        for x in alevels:
            if form.gradeField2.data == x[0]:
                stdgrades.append(x[1])
                break
        for x in alevels:
            if form.gradeField3.data == x[0]:
                stdgrades.append(x[1])
                break
        stdSum = 0
        for i in stdgrades:
            stdSum += i
        std.credits = stdSum
        # db.session.add(std)
        db.session.commit()
        return redirect(url_for("Qlf"))

    defaultChoice = [("NA","None")]
    if current_user.subject1 != "NA":
        print("subject1 is not none")
        print(current_user.subject1)
        form.subjectField1.default = current_user.subject1
        form.gradeField1.default = current_user.grade1
    else:
        form.subjectField2.default =  defaultChoice
        # form.gradeField2.default =
    if current_user.subject2 != "NA":
        print("subject2 is not none")
        print(current_user.subject2)
        form.subjectField2.default = current_user.subject2
        form.gradeField2.default = current_user.grade2
    else:
        form.subjectField1.default =  defaultChoice
        # form.gradeField1.default =
    if current_user.subject3 != "NA":
        print("subject3 is not none")
        print(current_user.subject3)
        form.subjectField3.default = current_user.subject3
        form.gradeField3.default = current_user.grade3
    else:
        form.subjectField1.default =  defaultChoice
    form.process()
        # form.gradeField1.default =
        # print(grade1)
        # print(std)

    return render_template('editQualifications.html', title = 'Qualifications Edit', form = form)
#

@app.route('/Qlf',methods = ['GET','POST'])
@login_required(role="Student")
def Qlf():
    std = models.User.query.filter_by(id = current_user.id,role = "Student").first()
    print(current_user.id)
    # print(std.subject1)
    # print(std.grade1)
    # print(std.subject2)
    # print(std.grade2)
    # print(std.subject3)
    # print(std.grade3)
    return render_template('qualifications.html', title = 'My Qualifications',student = std)



@app.route('/test')
# @login_required
def test():
    # create("equal")
    # create("more")
    # create("less")
    # print("HELLOO")
    unis=models.University.query.all()
    courses = models.Course.query.all()
    students = models.User.query.filter_by(role = "Student").all()
    # print(students)
    # for i in students:
    #     print(i)
    # for i in courses:
    #     print(i.University)
    # if current_user.is_authenticated:
    #     return redirect(url_for('workout'))
    return render_template('test.html', title = 'Test',university = unis, courses = courses, students = students)



#login page
@app.route('/login', methods=['GET', 'POST'])
def login():

    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm(request.form,meta={'csrf': True})
    if form.validate_on_submit():
        user = models.User.query.filter_by(email=form.email.data).first()
        if not authenticate.logInUser(form):
            flash('Invalid username or password')
        else:
            print("here")
            login_user(user, remember=form.remember.data)
            app.logger.info('User with id %s successfully logged in',user.id)
            next_page = request.args.get('next')
            if current_user.role == "Admin":
                return redirect(url_for('admin.index'))
            if not next_page or urlparse(next_page).netloc != '':
                app.logger.info('rerouting to home')
                next_page = url_for('home')
            return redirect(next_page)
            #return redirect(url_for('workout'))
    return render_template('security/login.html', title='Log In', form = form)


#register page
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = StudentForm(request.form,meta={'csrf': True})
    print("signup")
    if not form.validate_on_submit():
        print("not validated")
    if form.validate_on_submit():
        print("here")
        result,hashed  = authenticate.signup(form)
        if result is False:
            flash('An account already exists for this email')
        if result == True:
            print("creating user")
            authenticate.createUser(form, hashed)
            return redirect(url_for("login"))
    # if current_user.is_authenticated:#if already signed in go to homepage
    #     return redirect('/home')
    return render_template('security/signup.html', title='Sign Up', form=form)

#home route

@app.route('/home')
@login_required(role="Student")
def home():
    print("hello")
    app.logger.info('HOME')
    return render_template('home.html')


@app.route('/logout')
@login_required(role="ANY")
def logout():
    app.logger.info('Logging User id: %s Out', current_user.get_id())
    logout_user()#log user out
    return redirect('/')


def random_line(fileName):
    line = next(fileName)
    for num, aline in enumerate(fileName, 2):
        if random.randrange(num):
            continue
        line = aline
    return line

def createUniversities():
    count = 1
    courselist = []
    course =[]
    #create universities:
    with open('app/universities.csv', 'r') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            course = []
            for i in range(len(row)):
                #first value is university ranking
                if i == 0:
                    rank = row[i]
                    print("university rank:" + rank)
                #second value is university name
                elif i == 1:
                    uniName = row[i]
                    print("university Name:" + uniName)
                # third value is university website's url
                elif i == 2:
                    url = row[i]
                    print("university url:" + url)
                # all other values are course values
                else:
                    print(row[i])
                    course.append(row[i])
                    # count+=1
                    # if count !=6:
                    #     course.append(row[i])
                    #     print("course:" + row[i])
                    # else:
                    #     courselist.append(course)
                    #     print("Course details:" + str(course))
                    #     course = []
                    #     count = 0
            u = models.University(uniName = uniName,ranking = rank,url = url)
            db.session.add(u)
            db.session.commit()
            courses = np.array(course)
            print(str(courses))
            currentUni = models.University.query.filter_by(uniName = uniName).first()
            if len(courses) > 7:
                courselist =np.split(courses,2)
                for x in courselist:
                    courseRank = x[0]
                    coursename = x[1]
                    req1 = x[2]
                    req2 = x[3]
                    req3 = x[4]
                    criteria = x[5]
                    seats = int(x[6])
                    c = models.Course(rank = courseRank,CourseName = coursename, req1 = req1, req2 = req2, req3 = req3,criteria = criteria,seats = seats, university_id = currentUni.uniId)
                    db.session.add(c)
            else:
                courseRank = courses[0]
                coursename = courses[1]
                req1 = courses[2]
                req2 = courses[3]
                req3 = courses[4]
                criteria = courses[5]
                seats = courses[6]
                c = models.Course(rank = courseRank,CourseName = coursename, req1 = req1, req2 = req2, req3 = req3,criteria = criteria,seats = seats, university_id = currentUni.uniId)
                db.session.add(c)
            #create course

    db.session.commit()

# creates the number of students passed to it as parameter
def createStudents(population):
    alevels = ["A*","A","B","C","D"]
    # create a-levels array of tuples
    alvs = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
    # get all course subjects
    query = db.session.query(models.Course.CourseName.distinct().label("CourseName"))
    courseNames = [row.CourseName for row in query.all()]

    #initialise all files to be opened
    fileName = "app/names.txt"
    fileName2 = "app/surnames.txt"
    fileName3 = "app/alevels.txt"
    #make seed the current time
    random.seed(datetime.now())

    # open all files
    with open(fileName,'r') as n, open(fileName2, 'r') as sn, open(fileName3, 'r') as als:
        names = n.readlines()
        surnames = sn.readlines()
        alvls = als.readlines()
        studentNo = 0
        # # create students
        #for each name in names.txt
        for name in names:
            if studentNo ==population:
                break
                #for each surname in surnames.txt
            for surname in surnames:
                if studentNo == population:
                    break
                #get name and surname
                name = name.strip('\n')
                surname = surname.strip('\n')
                studentNo +=1
                #create email and password using name and surname
                email = name + surname + "@gmail.com"
                pwd = name + surname
                hshd = authenticate.hashPWD(pwd)

                l = []
                subject = []
                #get random A-level grades
                for x in range(3):
                    sub = random.choice(alevels)
                    l.append(sub.strip('\n'))
                # print(l)
                # fileName = "alevels.csv"
                subjectNo = 0
                #get random A-level subjects
                while subjectNo != 3:
                    sub = random.choice(alvls)
                    sub = sub.strip("\n")
                    if sub not in subject:
                        subject.append(sub.strip("\n"))
                        subjectNo+=1

                 #get random applied Course from db
                appliedCourse = random.choice(courseNames)

                # compute quantification of qualifications
                stdgrades = []
                for x in alvs:
                    # print(str(x[0]) + " == " + str(l[0]))
                    if l[0] == x[0]:
                        stdgrades.append(x[1])
                        break
                for x in alvs:
                    if l[1] == x[0]:
                        stdgrades.append(x[1])
                        break
                for x in alvs:
                    if l[2] == x[0]:
                        stdgrades.append(x[1])
                        break
                stdSum = 0
                for i in stdgrades:
                    stdSum += i
                print(studentNo)
                # print(stdSum)
                # print(subject)
                # print( name +" " + surname + " " + email + " " + str(subject))
                #create student object
                p = models.User(name = name,surname=surname, email = email, password = hshd,grade1 = l[0],subject1 = subject[0],grade2 = l[1],subject2 = subject[1], grade3 = l[2], subject3 = subject[2],appliedCourse = appliedCourse,credits =stdSum, role = "Student")    #surname = data.surname.data, DateOfBirth = data.DateOfBirth, CCD = hashedCCD, CVV = hashedCVV, DateOfExpire = data.DateOfExpire ,paymentIntervals = data.paymentIntervals
                db.session.add(p)
    db.session.commit()



def create(option):
    print("starting")
    a=models.University.query.filter_by(uniName="University of Leeds").first()

    if a is None:
        createUniversities()
        seats = 0
        courses = models.Course.query.all()
        for course in courses:
            seats += course.seats
        stdNo = seats
        print(stdNo)
        createStudents(stdNo)
        # print(studentPopulation)

        # fileName = "universities.csv"
        # string_to_add = ""
