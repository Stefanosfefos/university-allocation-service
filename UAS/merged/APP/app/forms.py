from flask_wtf import FlaskForm
from flask_login import current_user, logout_user, login_user
from wtforms import StringField, SubmitField , TextAreaField, BooleanField, SelectField, PasswordField ,DateField, IntegerField,validators, HiddenField
from wtforms.validators import DataRequired,Length,Email, EqualTo, ValidationError
from wtforms.fields.html5 import DateField, EmailField
from app import db,models
# from app.models import User


class StudentForm(FlaskForm):
      name= StringField('Name',
                               validators=[DataRequired(), Length(min=2, max=20)])

      surname=StringField('Surname',
                                validators=[DataRequired(), Length(min=2, max=20)])

      email = EmailField('Email address', [validators.DataRequired(), validators.Email(), validators.Length(min=6, max=35, message='Email must be between 6 and 35 characters')])

      password = PasswordField('Password', [validators.DataRequired(), validators.Length(min=4, max=35,message='Password must be between 4 and 35 characters')])

      confirm_password = PasswordField('Confirm Password',
                                       validators=[DataRequired(), EqualTo('password')])

      submit = SubmitField('Sign Up')

class QualificationForm(FlaskForm):

    subjectChoice = [("Latin", "Latin"), ("Classical Greek", "Classical Greek"), ("French", "French"), ("Italian", "Italian"), ("Classical Civilisations", "Classical Civilisations"), ("Law", "Law"), ("Music", "Music"), ("Philosophy", "Philosophy"), ("English Literature", "English Literature"),("English Language and Literature", "English Language and Literature"),
     ("History", "History"), ("Environmental Science", "Environmental Science"), ("Geography", "Geography"), ("Economics", "Economics"), ("Business Studies", "Business Studies"), ("Government and Politics", "Government and Politics"), ("Biology", "Biology"), ("Human Biology", "Human Biology"), ("Mathematics", "Mathematics"), ("Further Maths", "Further Maths"), ("Statistics", "Statistics"),
     ("Computing", "Computing"), ("Psychology", "Psychology"), ("Sociology", "Sociology"), ("Computer Science", "Computer Science"), ("Electronics", "Electronics"), ("Art", "Art"), ("Design and Technology", "Design and Technology"), ("Physics", "Physics"), ("Chemistry", "Chemistry"), ("History of Art", "History of Art"), ("Geology", "Geology"), ("ICT","ICT"), ("NA","None") ]
    gradeChoice = [("A*", "A*"), ("A", "A"), ("B", "B"), ("C", "C"), ("D", "D") ]
    defaultChoice = [("NA","None")]
    subjectField1 = SelectField(u'Subject', choices = subjectChoice, default = "NA" )
    gradeField1 = SelectField(u'Grade', choices = gradeChoice )
    subjectField2 = SelectField(u'Subject', choices = subjectChoice,default = "NA")
    gradeField2 = SelectField(u'Grade', choices = gradeChoice)
    subjectField3 = SelectField(u'Subject', choices = subjectChoice, default = "NA" )
    gradeField3 = SelectField(u'Grade', choices = gradeChoice)
    submit = SubmitField('Submit')
    def validate(self):
        if not FlaskForm.validate(self):
            return False
        result = True
        seen = set()
        for field in [self.subjectField1, self.subjectField2, self.subjectField3]:
            if field.data in seen:
                print(field.data)
                field.errors.append('Subject Duplicate: Please select distinct subjects.')
                result = False
            else:
                seen.add(field.data)
        return result

class ApplicationForm(FlaskForm):
    query = db.session.query(models.Course.CourseName.distinct().label("CourseName"))
    courses = [row.CourseName for row in query.all()]
    # courses = []
    c = []
    # print("here i am")
    for course in courses:
        c.append((course,course))
    courseField = SelectField(u'Courses', choices = c)
    submit = SubmitField('Apply')
# class Inputs(FlaskForm):
#     myChoices = [("Card", "Pay By Card"), ("Cash", "Pay By Cash") ]
#     freeChoice = [("Free", "Free")]
#     myField = SelectField(u'Free', choices = myChoices, default = "Free")
#     sessionId = HiddenField(validators = [DataRequired()])
#     submit = SubmitField('Book')

class ExecuteForm(FlaskForm):
    submit = SubmitField('Execute')

class ExecuteForm2(FlaskForm):
    myChoices = [("EF","Exact Fit"),("EFR","Exact Fit with Range"),("PR","Process"),("CR", "Course Rank Based"),]
    myField = SelectField(u'Select Algorithm', choices = myChoices, default = "PR")
    multiplier = IntegerField("Student Multiplier",default = 0)
    submit = SubmitField('Execute')


class Inputs2(FlaskForm):
    sessionId = HiddenField(validators = [DataRequired()])
    submit = SubmitField('Book')

# form that the user will use to log in
class LoginForm(FlaskForm):
    email = EmailField('Email address', [validators.DataRequired(), validators.Email(), validators.Length(min=6, max=35, message='Email must be between 6 and 35 characters')])

    password = PasswordField('Password', [validators.DataRequired(), validators.Length(min=4, max=35,message='Password must be between 4 and 35 characters')])

    remember = BooleanField('Remember Me')

    submit = SubmitField('Login')
