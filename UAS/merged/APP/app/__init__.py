from os.path import join, dirname, realpath
import os
from flask import Flask, url_for, redirect, render_template, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
# from flask_security import Security, SQLAlchemyUserDatastore, \
#     UserMixin, RoleMixin, login_required, current_user
# from flask_security.utils import encrypt_password
import flask_admin
from flask_admin import helpers as admin_helpers
from flask_migrate import Migrate
from flask_wtf import CsrfProtect
from flask_login import LoginManager
import stripe
import logging
from sqlalchemy.sql import func
from logging.handlers import RotatingFileHandler
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import _datetime



app = Flask(__name__)
app.config.from_object('config')
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login=LoginManager(app)
stripe_keys = {
    'secret_key': "sk_test_M3P2bVZ8VMVmyfnHvjmHiLhr00BeinvoQm",
    'publishable_key': "pk_test_kS2fgWK3bzaUmelIMambPIxc00KK3aljfB"
}

stripe.api_key = stripe_keys['secret_key']
CsrfProtect(app)

from flask_admin import AdminIndexView, expose

#Admin's email:sc18sc@leeds.ac.uk
        #password:12345

#generates income and usage for all facilities,sessions and activities, used as an index view for admins and employees
class MyHomeView(AdminIndexView):
    @expose('/')
    def index(self):
        alevels = [("A*",56),("A",48),("B",40),("C",24),("D",16)]
        # print(current_user.appliedCourse)
        # for i in range(26):
        #     print(str(i) + " " + str((i*17)%26))
        # kll = models.Course.query.filter()
        # ccc = 0
        # kl = models.User.query.filter(models.User.enrolledCourse != None, models.User.role == "Student").all()
        # for i in kl:
        #     cou = models.Course.query.filter_by(courseId = i.enrolledCourse).first()
        #     if i.appliedCourse == cou.CourseName:
        #         ccc+=1
        # print("total: " + str(ccc))
        # sss = db.session.query(func.sum(models.Course.seats).label('sum'))
        # print(sss.fetch())
        CSApplicants = models.User.query.filter_by(appliedCourse = "Computer Science").count()
        ECApplicants = models.User.query.filter_by(appliedCourse = "Economics").count()
        # get all students that applied to a course
        studentNo = models.User.query.filter(models.User.appliedCourse != None,models.User.role == "Student").count()
        print(studentNo)
        # get all students that got enrolled at a course
        total_enrolled = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").count()
        #calculate percentage of students enrolled from those applied
        if studentNo != 0:
            penrolled = total_enrolled/studentNo * 100
        else:
            penrolled =0
        penrolled = str(penrolled) + "%"
        #get enrolled studens
        enrolledStudents = models.User.query.filter(models.User.enrolledCourse != None,models.User.role == "Student").all()

        # count number of universities
        uniNo = models.University.query.count()
        # students = models.Student.query.filter_by(models.Student.enrolledCourse !=Nonw)
        #get all universities and courses
        unis = models.University.query.all()
        courses = models.Course.query.all()
        #initialise counters for:
        #number of students that fulfill course criteria or have higher criteria than course requests
        nStudentsFulfillorHigher = 0
        #number of students that have qualifications equal to course criteria
        nStudentsFulfill = 0

        # for student in enrolledStudents:
        for student in enrolledStudents:
            enrolledCourse = student.course
            if enrolledCourse != None:
                courseCriteria =[enrolledCourse.req1,enrolledCourse.req2,enrolledCourse.req3]
                studentCriteria = [student.grade1,student.grade2,student.grade3]
                courseCriteria.sort()
                studentCriteria.sort()
                # if their qualifications match course criteria exactly +1
                if  courseCriteria ==studentCriteria:
                    nStudentsFulfill+=1
                # calculate grades for student
                # studentgrades = []
                # for x in alevels:
                #     if student.grade1 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade2 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade3 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # studentSum = 0
                # for i in studentgrades:
                #     studentSum += i
                studentSum = student.credits
                #calculate sum of grades student needs for this course
                coursegrades = []
                for x in alevels:
                    if enrolledCourse.req1 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if enrolledCourse.req2 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if enrolledCourse.req3 == x[0]:
                        coursegrades.append(x[1])
                        break
                gradeSum = 0
                for i in coursegrades:
                    gradeSum += i
                #if student's qualifications are greater than course's increment counter
                if studentSum >=gradeSum:
                    nStudentsFulfillorHigher +=1
        seats = 0
        # studentsEnrolledAtAppliedCourse = 0
        courseData = []
        nStudentsmatch = []
        #for each course:
        for course in courses:
            # calculate:
            #number of students that match a course exactly
            nStudentsExactMatchAcourse = 0
            #number of students that match a course or higher
            nStudentsMatchAcourse = 0
            #number of students that match a the course they enrolled at or higher
            nStudentsEnrolledAtMatchedCourse = 0
            #number of students that match the course they enrolled at exactly
            nStudentsEnrolledAtExactMatchedCourse = 0
            #number of students enrolled at the course
            studentsEnrolledAtAppliedCourse = 0

            #get all students enrolled at current course
            # sEnrolled = models.User.query.filter_by(enrolledCourse = course.courseId,role = "Student").all()
            # get number of students enrolled at current course
            studentsEnrolled = models.User.query.filter(models.User.enrolledCourse == course.courseId,models.User.role == "Student").count()
            # get number of students that applied for this course subject
            nStudentsApplied = models.User.query.filter_by(appliedCourse = course.CourseName,role = "Student").all()
            seats += course.seats

            # for each student enrolled at this course:
            for student in course.students:
                # print(student)
                # print("HEREEE")
                # calculate number of students that match course criteria or higher
                stdgrades = [student.grade1, student.grade2, student.grade3]
                coursereq = [course.req1, course.req2, course.req3]
                coursereq.sort()
                stdgrades.sort()
                if coursereq == stdgrades:
                    # print("exactmatch")
                    nStudentsEnrolledAtExactMatchedCourse+=1

                studentSum = student.credits
                # studentgrades = []
                # for x in alevels:
                #     if student.grade1 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade2 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade3 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # studentSum = 0
                # for i in studentgrades:
                #     studentSum += i

                coursegrades = []
                for x in alevels:
                    if course.req1 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if course.req2 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if course.req3 == x[0]:
                        coursegrades.append(x[1])
                        break
                gradeSum = 0
                for i in coursegrades:
                    gradeSum += i
                if studentSum >=gradeSum:
                    # print("range match")
                    nStudentsEnrolledAtMatchedCourse +=1

            # for each student that applied to this course:
            for student in nStudentsApplied:

                # print(course)
                # print(student)
                stdgrades = [student.grade1, student.grade2, student.grade3]
                coursereq = [course.req1, course.req2, course.req3]
                coursereq.sort()
                stdgrades.sort()
                if coursereq == stdgrades:
                    nStudentsExactMatchAcourse+=1
                #
                studentSum = student.credits
                # studentgrades = []
                # for x in alevels:
                #     if student.grade1 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade2 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # for x in alevels:
                #     if student.grade3 == x[0]:
                #         studentgrades.append(x[1])
                #         break
                # studentSum = 0
                # for i in studentgrades:
                #     studentSum += i
                coursegrades = []
                for x in alevels:
                    if course.req1 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if course.req2 == x[0]:
                        coursegrades.append(x[1])
                        break
                for x in alevels:
                    if course.req3 == x[0]:
                        coursegrades.append(x[1])
                        break
                gradeSum = 0
                for i in coursegrades:
                    gradeSum += i
                #increment the number of students that applied for this course and had enough or more qualifications
                if studentSum >=gradeSum:
                    # print(str(studentSum) + ">=" + str(gradeSum) )
                    nStudentsMatchAcourse +=1
                #count number of students that applied to this course
                if student.enrolledCourse != None:
                    if student.appliedCourse == student.course.CourseName:
                        studentsEnrolledAtAppliedCourse += 1


            courseData.append((course.rank,course.University,course.CourseName,studentsEnrolled, course.seats,nStudentsExactMatchAcourse, nStudentsMatchAcourse,nStudentsEnrolledAtExactMatchedCourse,nStudentsEnrolledAtMatchedCourse))
        print(courseData)



        avgpSeatsperCourseFulfilled =0
        for ci in courseData:
            avgpSeatsperCourseFulfilled += ci[8] /ci[4]

        ncourses = models.Course.query.count()
        # ncourses = 59

        avgpSeatsperCourseFulfilled = str(round(avgpSeatsperCourseFulfilled/ncourses * 100)) + "%"

        #calculate percentage of students that match course criteria exactly
        if studentNo != 0:
            # pStudentsFulfilled = str(round(nStudentsFulfill/total_enrolled * 100)) + "%"
            # pStudentsFulfilledorHigher = str(round(nStudentsFulfillorHigher/total_enrolled * 100)) + "%"
            pSeatsFulfilled = str(round(nStudentsFulfill/seats * 100)) + "%"
            pSeatsFulfilledorHigher = str(round(nStudentsFulfillorHigher/seats * 100)) + "%"
        else:
            # pStudentsFulfilled = 0
            # pStudentsFulfilledorHigher = 0
            pSeatsFulfilled = 0
            pSeatsFulfilledorHigher = 0
        # allStudents = models.User.query.filter_by(role = "Student").all()
        # for s in allStudents:
        #     print(s.preferences)
        return self.render('admin/index.html',
                                title = 'Algorithm results',
                                total = studentNo,
                                total_enrolled = total_enrolled,
                                percentage_enrolled = penrolled,
                                uniNo = uniNo,
                                ncourses = ncourses,
                                avgpSeatsperCourseFulfilled = avgpSeatsperCourseFulfilled,
                                nStudentsFulfill = nStudentsFulfill,
                                nStudentsFulfillorHigher = nStudentsFulfillorHigher,
                                seats = seats,
                                # pStudentsFulfilled = pStudentsFulfilled,
                                # pStudentsFulfilledorHigher = pStudentsFulfilledorHigher,
                                pSeatsFulfilled = pSeatsFulfilled,
                                pSeatsFulfilledorHigher = pSeatsFulfilledorHigher,
                                courseData = courseData,
                                CSApplicants = CSApplicants,
                                ECApplicants = ECApplicants)




admin = Admin(
        app,
        name = 'University Enrollment Service',
        # base_template = 'layout.html',
        template_mode='bootstrap3',
        index_view = MyHomeView()
        )
# from app import authenticate
# if models.User.query.filter_by(role ="Admin").first() is None:
#     createAdmin("Stefanos","Costa","sc18sc@leeds.ac.uk","12345")
from .forms import ExecuteForm
from app import views,models






# from app.models import Facility

#logging system:
if not app.debug:

    if not os.path.exists('logs'):
        os.mkdir('logs')
    format = logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    file_handler = RotatingFileHandler('logs/services.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(format)
    file_handler.setLevel(logging.INFO)

    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)

    app.logger.info('University Service startup')
