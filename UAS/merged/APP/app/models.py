from app import db,admin
from datetime import datetime,date
from flask import url_for,redirect,request,abort, render_template, request, flash
import flask_wtf
from flask_admin.model.form import InlineFormAdmin
from flask_admin.model import typefmt
# from flask_security import Security, SQLAlchemyUserDatastore, \
#     UserMixin, RoleMixin, login_required, current_user
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin import Admin, BaseView, expose, AdminIndexView
from flask_login import UserMixin, login_required, current_user
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.view import func
from flask_admin.menu import MenuLink
import hashlib
# from app import views
# from app.forms import ExecuteForm
# from flask_security import utils
from app import login
from os import path


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


# for every university there are many students
# for every student there is one university
class University(db.Model):
    uniId = db.Column(db.Integer(), primary_key=True)
    uniName = db.Column(db.String(80), unique = True)
    ranking = db.Column(db.Integer(), unique = True)
    url = db.Column(db.String(100))
    course = db.relationship("Course", uselist = True, backref = "University")

    def __repr__(self):
        return '{}'.format(self.uniName)

    # courses =
class User(db.Model,UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(30))
    surname = db.Column(db.String(30))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    idNo = db.Column(db.String(30), unique = True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    grade1 = db.Column(db.String(2))
    subject1 = db.Column(db.String(25))
    grade2 = db.Column(db.String(2))
    subject2 = db.Column(db.String(25))
    grade3 = db.Column(db.String(2))
    subject3 = db.Column(db.String(25))
    preferences = db.Column(db.String(250))
    pLength = db.Column(db.Integer())
    index = db.Column(db.Integer(), default = 0)
    appliedCourse = db.Column(db.String(25))
    credits = db.Column(db.Integer(), default = 0)
    enrolledCourse = db.Column(db.Integer(),db.ForeignKey("course.courseId"))
    role = db.Column(db.String(15))
#
    def getRoles(self):
            return self.role

    def __repr__(self):
        return '{} {} {}'.format(self.name, self.surname,self.id)

# class Student(db.Model,UserMixin):
#     id = db.Column(db.Integer(), primary_key=True)
#     studentName = db.Column(db.String(30))
#     studentSurname = db.Column(db.String(30))
#     idNo = db.Column(db.String(30), unique = True)
#     email = db.Column(db.String(255), unique=True)
#     password = db.Column(db.String(255))
#     grade1 = db.Column(db.String(2))
#     subject1 = db.Column(db.String(25))
#     grade2 = db.Column(db.String(2))
#     subject2 = db.Column(db.String(25))
#     grade3 = db.Column(db.String(2))
#     subject3 = db.Column(db.String(25))
#     preferences = db.Column(db.String(250))
#     index = db.Column(db.Integer(), default = 0)
#     appliedCourse = db.Column(db.String(25))
#     credits = db.Column(db.Integer(), default = 0)
#     enrolledCourse = db.Column(db.Integer(),db.ForeignKey("course.courseId"))
#
#     def __repr__(self):
#         return '{} {} {}'.format(self.studentName, self.studentSurname,self.id)


class Course(db.Model):
    courseId = db.Column(db.Integer(), primary_key=True)
    CourseName = db.Column(db.String(30))
    rank = db.Column(db.Integer(), default = 0)
    req1 = db.Column(db.String(2))
    req2 = db.Column(db.String(2))
    req3 = db.Column(db.String(2))
    criteria = db.Column(db.String(25))
    seats = db.Column(db.Integer())
    availability = db.Column(db.Integer(), default = 0)
    university_id = db.Column(db.Integer(),db.ForeignKey("university.uniId"))
    students =  db.relationship("User", uselist = True, backref = "course", lazy = 'dynamic')

    def __repr__(self):
        return '{} {} {}'.format(self.University, self.CourseName,self.courseId)



#
class AdminUserView(ModelView):


    def on_model_change(self, form, User, is_created):
        User.password = hashlib.md5((User.password).encode()).hexdigest()
        db.session.commit()



    def is_visible(self):
        return current_user.role == 'Admin'

    column_filters = ['role']
    create_modal = True
    edit_modal = True
    column_exclude_list = ['password' ]
    column_searchable_list = ['name', 'surname', 'email']
    # column_editable_list = ['is_banned']


    def is_accessible(self):
         if current_user.is_authenticated:
             if current_user.role == "Admin" or current_user.role == "Employee":

                 return (current_user.is_active and
                         current_user.is_authenticated)
             else:
                 return abort(403)
         else:
             return abort(403)
#
admin.add_view(AdminUserView(User, db.session,endpoint = "Admin Users"))
admin.add_link(MenuLink(name='Logout', category='', url="/logout"))
